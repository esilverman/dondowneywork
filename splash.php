<?php
/**
Template Name: DonDowney Home

 */

get_header();

$the_title = get_the_title("","",false);

global $isHomePage;
$isHomePage = true;
?>

		<div id="homePage_image"><img src="<?php bloginfo('template_url');?>/images/home.png"/></div>


<?php //get_sidebar(); ?>

<?php get_footer(); ?>

	<div id="content" role="main">

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	<?php the_content(); ?>
<?php endwhile; endif; ?>
	</div>
