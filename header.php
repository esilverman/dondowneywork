<?php
/**
 * @package Decubing
 * @subpackage Default_Theme
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>

<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />

<title><?php wp_title('&laquo;', true, 'right'); ?> <?php bloginfo('name'); ?></title>

<link href="http://fonts.googleapis.com/css?family=Andada" rel="stylesheet" type="text/css">

<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.14/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.mousewheel.3.1.4.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.sbscroller-1.6.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/dd.js"></script>

<script type="text/javascript">
  $(document).ready(function(){
   	$(function(){ $('.populate').each(function(){ var e = $(this); if (e.attr('title').length &! e.val().length) { e.val(e.attr('title')); } if (e.attr('title') == e.val()) e.addClass('inputLabel'); e.focus(function(){ var e = $(this); if (e.attr('title') == e.val()) { e.val('').removeClass('inputLabel'); } }).blur(function(){ var e = $(this); if (e.attr('title').length &! e.val().length) { e.val(e.attr('title')).addClass('inputLabel'); } }); $('label[@for='+$(this).attr('id')+']').addClass('structural'); }); });
  });
 </script>


<script type="text/javascript">
sfHover = function() {
	var sfEls = document.getElementById("navbar").getElementsByTagName("li");
	for (var i=0; i<sfEls.length; i++) {
		sfEls[i].onmouseover=function() {
			this.className+=" hover";
		}
		sfEls[i].onmouseout=function() {
			this.className=this.className.replace(new RegExp(" hover\\b"), "");
		}
	}
}
if (window.attachEvent) window.attachEvent("onload", sfHover);

sfHover = function() {
   var sfEls = document.getElementById("navbar").getElementsByTagName("li");
   for (var i=0; i<sfEls.length; i++) {
      sfEls[i].onmouseover=function() {
         this.className+=" hover";
      }
      sfEls[i].onmouseout=function() {
         this.className=this.className.replace(new RegExp(" hover\\b"), "");
      }
   }
}
if (window.attachEvent) window.attachEvent("onload", sfHover);
</script>

<?php wp_head(); ?>
<?php

$the_title = strtolower( trim( wp_title("",false) ) );

if ($the_title == "" || $the_title == "don-downey.com") { $the_title = "home";}


$ua=getBrowser();
$yourbrowser= "Your browser: " . $ua['name'] . " " . $ua['version'] . " on " .$ua['platform'] . " reports: <br >" . $ua['userAgent'];
/* echo "<pre>".print_r($ua,true)."</pre>"; */
?>
</head>
<body id="<?php echo $the_title; ?>_body" <?php body_class(); ?>>

<?php
	$contactPageId = get_page_by_title( 'Contact' );
	$homePageId = get_page_by_title( 'Home' );
?>
<div id="page" class="<?php echo $ua["engine"] == "Webkit" ? "webkit" : "non-webkit"; ?>">

