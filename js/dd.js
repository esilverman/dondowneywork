var $ = jQuery;
$(function () {
	var headerWidth = pageWidth = 1060,
			headerCharsWidth = 0,
			numHeaderChars = $('.header_char').size(),
			headerCharMargin, footerMenuItemMargin,
			uniqueChars = {
				film:			1,
				profile:	2,
				fiction:	5,
				poetry:		1,
				contact:	1
			},
			footerMenuItemWidth = 0;
	
/* 	for (var i = 0; i <= numHeaderChars; i++) {} */

	$('.page_header .header_char').each(function() {
    headerCharsWidth += $(this).outerWidth( true );
	});

	headerExtraSpace = headerWidth - headerCharsWidth;
	
	headerCharMargin = parseInt( (headerExtraSpace / (numHeaderChars-1) ) / 2, 10 );
	
	$('.header_link').hover(  function () {
    $(this).toggleClass("header_link_hover");
  });	
		
	$('.page_header .header_char').not(':first').not(':last').each(function() {
		$(this).css({textAlign: 'center'})
			.width($(this).width() + (headerCharMargin*2) );
	});
	
	
	$('.header_char:first')
		.width($('.header_char:first').outerWidth( true ) + headerCharMargin)
		.css({textAlign:'left'});
	
	
	$('.header_char:last')
		.width($('.header_char:last').outerWidth( true ) + headerCharMargin)
		.css({textAlign:'right'});
		

	$('#footer .menu ul li a').each (function () {
				
		var title = $(this).html().toLowerCase(),
				newTitle = '';
		for ( var i = 0; i < title.length; i++ ) {
		 	newTitle += uniqueChars[title] == i ? '<span class="unique_char">'+title[i]+'</span>' : '<span>'+title[i]+'</span>';
		}
		
		$(this).html(newTitle).closest('li').addClass(title+'_footer_link').find('span:last').addClass('footer_last_char');	
		
	});
	
	// Film page behaviors
	
/*
	$('.entry')
		.hover( 
			function () {
		    $(this).find('.overlay_arrow').removeClass("hidden");
		  },
			function () {
		    $(this).find('.overlay_arrow').addClass("hidden");
		  }
	  );	
*/

	$('.entry')
		.hover( 
			function () {
		    $(this).find('.fancybox').css({backgroundPosition: "0px 0px"});
		  },
			function () {
		    $(this).find('.fancybox').css({backgroundPosition: "0px -130px"});
		  }
	  );	
		
	// Start Footer menu distribution
		
	var numFooterItems = 0;
	$('#footer .menu li').each(function() {
    footerMenuItemWidth += $(this).width();
    numFooterItems++;
	});

	footerExtraSpace = pageWidth - footerMenuItemWidth;
	footerMenuItemMargin = parseInt( (footerExtraSpace / (numFooterItems-1) ) / 2, 10);
	numMarginsToModify = (numFooterItems-1) * 2;
	extraMargin = footerExtraSpace - (footerMenuItemMargin * numMarginsToModify ) ;

	//console.log({footerExtraSpace : footerExtraSpace, footerMenuItemMargin : footerMenuItemMargin, extraMargin:extraMargin, numMarginsToModify:numMarginsToModify});

	$('#footer .menu li').not(':first').not(':last').each(function() {
		$(this).css({textAlign: 'center'})
			.css({marginRight: footerMenuItemMargin, marginLeft: footerMenuItemMargin} );
	});

	$('#footer .menu li:first').css({marginRight: footerMenuItemMargin + parseInt(extraMargin/2, 10) });
	$('#footer .menu li:last').css({marginLeft: footerMenuItemMargin+ parseInt(extraMargin/2, 10) });

	// End Footer menu distribution
	
/*
  $(window).load(function(){
  	$('#poetry_body .entry').mCustomScrollbar({

  	});		
	});
*/

	$('#poetry_body .entry').sbscroller({				
				mousewheel: true,
				autohide:false
	});
	
});