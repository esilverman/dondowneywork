<?php

if ($_SERVER['HTTP_HOST'] !== 'localhost:8888') :
  require_once dirname( __FILE__ ) . '/core/custom-fields.php'; // custom fields
endif;

/* require_once dirname( __FILE__ ) . '/core/scripts-styles.php'; // Scripts & Styles */

$content_width = 450;

automatic_feed_links();

if ( function_exists('register_sidebar') ) {
	register_sidebar(array(
		'before_widget' => '<li id="%1$s" class="widget %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h2 class="widgettitle">',
		'after_title' => '</h2>',
	));
}


function getBrowser() 
{ 
    $u_agent = $_SERVER['HTTP_USER_AGENT']; 
    $bname = 'Unknown';
    $platform = 'Unknown';
    $version= "";

    //First get the platform?
    if (preg_match('/linux/i', $u_agent)) {
        $platform = 'linux';
    }
    elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
        $platform = 'mac';
    }
    elseif (preg_match('/windows|win32/i', $u_agent)) {
        $platform = 'windows';
    }
    
    // Next get the name of the useragent yes seperately and for good reason
    if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent)) 
    { 
        $bname = 'Internet Explorer'; 
        $ub = "MSIE"; 
    } 
    elseif(preg_match('/Firefox/i',$u_agent)) 
    { 
        $bname = 'Mozilla Firefox'; 
        $ub = "Firefox"; 
    } 
    elseif(preg_match('/Chrome/i',$u_agent)) 
    { 
        $bname = 'Google Chrome'; 
        $ub = "Chrome";
        $engine = "Webkit";
    } 
    elseif(preg_match('/Safari/i',$u_agent)) 
    { 
        $bname = 'Apple Safari'; 
        $ub = "Safari"; 
        $engine = "Webkit";
    } 
    elseif(preg_match('/Opera/i',$u_agent)) 
    { 
        $bname = 'Opera'; 
        $ub = "Opera"; 
    } 
    elseif(preg_match('/Netscape/i',$u_agent)) 
    { 
        $bname = 'Netscape'; 
        $ub = "Netscape"; 
    } 
    
    // finally get the correct version number
    $known = array('Version', $ub, 'other');
    $pattern = '#(?<browser>' . join('|', $known) .
    ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
    if (!preg_match_all($pattern, $u_agent, $matches)) {
        // we have no matching number just continue
    }
    
    // see how many we have
    $i = count($matches['browser']);
    if ($i != 1) {
        //we will have two since we are not using 'other' argument yet
        //see if version is before or after the name
        if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
            $version= $matches['version'][0];
        }
        else {
            $version= $matches['version'][1];
        }
    }
    else {
        $version= $matches['version'][0];
    }
    
    // check if we have a number
    if ($version==null || $version=="") {$version="?";}
    
    return array(
        'userAgent' => $u_agent,
        'name'      => $bname,
        'version'   => $version,
        'platform'  => $platform,
        'pattern'   => $pattern,
        'engine'		=> $engine
    );
} 

function print_header_title ($the_title, $echo = TRUE) {
	$titleArray = str_split($the_title);
	
	$headerText = "";
	
	$uniqueChars = array(      
		'home'			=> -1,
		'film'    	=> 1,
		'bio'     	=> 1,
		'fiction' 	=> 5,
		'poetry'  	=> 1,
		'contact' 	=> 1,
		'trailers'	=> -1
	);
	
	$headerTable = "";
	$i = 0;
	foreach($titleArray as $char) {
		$uniqueCharClass = $i++ == $uniqueChars[strtolower($the_title)] ? "unique_char" : "";

		$lastCharClass = !isset($titleArray[$i]) ? "last_char": "";

		$headerText .= "<div class='header_char char_".$i." $uniqueCharClass $lastCharClass'>$char</div>";
		$headerTable .= "<td>$char</td>";
	}
	
	$headerText = strtoupper($the_title) == "TRAILERS"
		? $headerText
		: "<a class='header_link' href='".get_bloginfo('home')."'>$the_title</a>";

	if ($echo) { echo $headerText; }

	return $headerText;
}

function print_footer_title ($the_title, $echo = TRUE) {
	$titleArray = str_split($the_title);
	
	$footerText = "";
	
	$uniqueChars = array(      
		'home'			=> -1,
		'film'    	=> 1,
		'bio'     	=> 1,
		'fiction' 	=> 5,
		'poetry'  	=> 1,
		'contact' 	=> 1,
		'trailers'	=> -1
	);
	
	$i = 0;
	foreach($titleArray as $char) {
		$uniqueCharClass = $i++ == $uniqueChars[strtolower($the_title)] ? "unique_char" : "";

		$lastCharClass = !isset($titleArray[$i]) ? "last_char": "";

		$footerText .= "<div class='footer_char char_".$i." $uniqueCharClass $lastCharClass'>$char</div>";
		$footerTable .= "<td>$char</td>";
	}
	
	$footerText = strtoupper($the_title) == "TRAILERS" ? $footerText : "TRAILERS";

	if ($echo) { echo $footerText; }

	return $footerText;
}

// Vimeo Code
 
define("VIMEO_WIDTH", 400); // default width
define("VIMEO_HEIGHT", 225); // default height
define("VIMEO_THUMB","http://dondowneywork.com/wp-content/uploads/2011/12/film.placeholder.png"); // default thumb
define("VIMEO_REGEXP", "/\[vimeo ([[:print:]]+)\]/");
/* define("VIMEO_TARGET", "<iframe class=\"fancybox-vimeo\" src=\"http://player.vimeo.com/video/###URL###\" width=\"###WIDTH###\" height=\"###HEIGHT###\" frameborder=\"0\"></iframe>"); */
define("VIMEO_TARGET", "<div class=\"vimeo_thumb\" style=\"background-image: url('###THUMB###');\"><a href=\"http://player.vimeo.com/video/###URL###\" width=\"###WIDTH###\" height=\"###HEIGHT###\" class=\"fancybox\"></a></div>");

// To add dimensions for vimeo popup...include "{width:600,height:500}" with a SPACE after class name of <a> tag


function dd_vimeo_plugin_callback($match)
{
	$tag_parts = explode(" ", rtrim($match[0], "]"));
	$output = VIMEO_TARGET;
	$output = str_replace("###URL###", $tag_parts[1], $output);
	if (count($tag_parts) > 2) {
	
		if ($tag_parts[2] == 0) {
			$output = str_replace("###WIDTH###", VIMEO_WIDTH, $output);
		} else {
			$output = str_replace("###WIDTH###", $tag_parts[2], $output);
		}
		
		if ($tag_parts[3] == 0) {
			$output = str_replace("###HEIGHT###", VIMEO_HEIGHT, $output);
		} else {
			$output = str_replace("###HEIGHT###", $tag_parts[3], $output);
		}

		if ( count($tag_parts[4]) == 0) {
			$output = str_replace("###THUMB###", VIMEO_THUMB, $output);
		} else {
			$output = str_replace("###THUMB###", $tag_parts[4], $output);
		}
		
	} else {
		$output = str_replace("###WIDTH###", VIMEO_WIDTH, $output);
		$output = str_replace("###HEIGHT###", VIMEO_HEIGHT, $output);	
	}
	return ($output);
}
function dd_vimeo_plugin($content)
{
	return (preg_replace_callback(VIMEO_REGEXP, 'dd_vimeo_plugin_callback', $content));
}

add_filter('the_content', 'dd_vimeo_plugin');
add_filter('the_content_rss', 'dd_vimeo_plugin');
add_filter('comment_text', 'dd_vimeo_plugin');
add_filter('the_excerpt', 'dd_vimeo_plugin');



?>
