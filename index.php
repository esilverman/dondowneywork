<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */

get_header();

$style_classes = array('thumb_first','thumb_middle','thumb_middle','thumb_last');
$styles_count = count($style_classes);
$style_index = 0;

$the_title = "FILM";

?>
	<div id="content" class="narrowcolumn" role="main">
		<div class="page_header <?php echo strtolower($the_title);?>_header"><?php print_header_title($the_title);?></div>
<!-- 		<div id="trailers_subheader" class="page_header"><?php //print_header_title("TRAILERS");?></div> -->
	<?php if (have_posts()) : $counter = 0;?>
		<?php //the_content(); ?>
	<div id="film_thumbs">
		<?php while (have_posts()) : the_post(); $counter++; ?>


		<div <?php post_class($style_classes[$style_index++ % $styles_count]) ?> id="post-<?php the_ID(); ?>">
			<div class="entry">
				<div class="the_content film">
  				<h2 class="the_title"><?php the_title(); ?></h2>
				  <?php if (get_field('vimeo_id') ) : ?>
            <?php if (get_field('thumb')) :
                $thumb = get_field('thumb');
                $thumb = $thumb['sizes']['thumbnail'];
              else :
                $thumb = "";
              endif;
          ?>
          <div class="vimeo_thumb" style="background-image: url('<?php echo $thumb; ?>');">
            <a href="http://player.vimeo.com/video/<?php the_field('vimeo_id'); ?>" width="<?php the_field('width'); ?>" height="<?php the_field('height'); ?>" class="fancybox fancybox-vimeo" style="background-position: 0px -130px;"></a>
          </div>
				  <?php endif; ?>
				  <?php if (get_field('details')) : ?>
				      <p class="details"><?php the_field('details'); ?></p>
				  <?php endif; ?>
				  <?php //the_content(); ?>
				</div>
				<img src="<?php bloginfo('template_url');?>/images/film.overlay.png" class="hidden overlay_arrow"/>
			</div>
<!-- 			<p class="postmetadata"><?php the_tags('Tags: ', ', ', '<br />'); ?> Posted in <?php the_category(', ') ?> | <?php edit_post_link('Edit', '', ' | '); ?>  <?php comments_popup_link('No Comments &#187;', '1 Comment &#187;', '% Comments &#187;'); ?></p> -->
		</div>
			<?php if ($counter % 4 == 0) : ?>
			  <div class="clearfix"></div>
			<?php endif; ?>
	
	
		<?php endwhile; ?>
	</div>
	
		<div class="navigation">
			<div class="alignleft"><?php next_posts_link('&laquo; Older Entries') ?></div>
			<div class="alignright"><?php previous_posts_link('Newer Entries &raquo;') ?></div>
		</div>

	<?php else : ?>

		<h2 class="center">Not Found</h2>
		<p class="center">Sorry, but you are looking for something that isn't here.</p>
		<?php get_search_form(); ?>

	<?php endif; ?>

	</div>

<?php //get_sidebar(); ?>

<?php get_footer(); ?>
