<?php
function client_scripts() {

  define('THEME_URL', get_bloginfo('stylesheet_directory').'/');

  if( !is_admin()){

/*     wp_deregister_script('jquery'); */
    wp_register_script('jquery-min',  ('http://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js'), false, '1.8.1# 4');
    wp_enqueue_script('jquery-min');
   
/*
  	wp_enqueue_script(
  		'scrollbar-mousewheel',
  		THEME_URL . "lib/m-custom-scrollbar/js/minified/jquery.mousewheel.min.js",
  		array('jquery')
  	);

  	wp_enqueue_script(
  		'mcustom-scrollbar',
  		THEME_URL . "lib/m-custom-scrollbar/js/minified/jquery.mCustomScrollbar.min.js",
  		array('jquery')
  	);
*/
      
  } // not admin
  
	wp_enqueue_script(
	  'jqueryui',
	  'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.14/jquery-ui.min.js',
	  array('jquery-min')
  );

	wp_enqueue_script(
	  'sbscroller-mousewheel',
		THEME_URL . "js/jquery.mousewheel.min.js",
	  array('jquery-min')
  );

	
	wp_enqueue_script(
		'sbscroller',
		THEME_URL . "js/jquery.sbscroller.js",
		array('jquery-min')
	);
	
  wp_enqueue_script(
  	'dd',
  	THEME_URL . "js/dd.js",
  	array('jquery-min')
  );
	
  wp_register_script('jquery',  ('http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js'), false, '1.8.1# 4');
  wp_enqueue_script('jquery');

// <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js"></script>
// <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.14/jquery-ui.min.js"></script>
// <script type="text/javascript" src="/js/jquery.mousewheel.min.js"></script>
// <script type="text/javascript" src="/js/jquery.sbscroller.js"></script>
// <script type="text/javascript" src="/js/dd.js"></script>

  

    
}    

add_action('wp_enqueue_scripts', 'client_scripts');
