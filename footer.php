<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */
?>
<?php
$contactPageId = get_page_by_title( 'Contact' );
?>

<hr />
	<div id="footer" role="contentinfo">
		
	<div id="navigation">
		<ul id="main">
	  <?php
	  $the_ID = is_home() ? $filmPage->ID : get_the_ID();

	  if ( is_home() ) {
	  	$queryArgs = array(
	  		"name" => "film",
	  		'post_type' => 'page'
	  	);
	  	$posts = get_posts($queryArgs);
	  	$filmPage = $posts[0];
	  	$the_ID = $filmPage->ID;
	  }

	  $menuArgs = array (
	  	"show_home" => "0",	
	  	"title_li" => "",	
	  	"meta_key" => "display",	
	  	"meta_value" => "navigation",	
	  	"exclude" => "$the_ID"
	  );
	  
/*
	  global $isHomePage;
	  if ( !$isHomePage  ) {
		  wp_page_menu($menuArgs);
		  //wp_list_pages("title_li=&meta_key=display&meta_value=navigation&exclude=$the_ID" );
	  }
*/

	  wp_page_menu($menuArgs);
	  ?>
	  </ul>

	</div>
	<div id="footer_info">&copy; <?php echo date("Y"); ?> Don Downey. All Rights Reserved. Made by <a href="http://madebycaliper.com/">CALIPER</a></div>

	</div>
</div>

		<?php wp_footer(); ?>
</body>
</html>
