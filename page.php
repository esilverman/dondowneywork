<?php
/**
 * @package Decubing
 * @subpackage Default_Theme
 */

get_header();

$the_title = get_the_title("","",false);

?>

	<div id="content" role="main">
		<div class="page_header <?php echo strtolower($the_title);?>_header"><?php print_header_title($the_title);?></div>
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<div class="post" id="post-<?php the_ID(); ?>">
			<div class="entry">
				<?php the_content('<p class="serif">Read the rest of this page &raquo;</p>'); ?>

				<?php wp_link_pages(array('before' => '<p><strong>Pages:</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>

			</div>
		</div>
		<?php endwhile; endif; ?>
	<?php //edit_post_link('Edit this entry.', '<p>', '</p>'); ?>
	
	
	</div>

<?php //get_sidebar(); ?>

<?php get_footer(); ?>
